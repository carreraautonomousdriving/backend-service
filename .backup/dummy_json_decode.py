from TrackInstruction import TrackInstruction
from RaceInstructionSet import RaceInstructionSet
import pickle


ris = RaceInstructionSet(2) #Roundcount
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.addInstruction(TrackInstruction(0, 200))
ris.ResetIndex = 5


ris.serialize('/opt/arerrac/raceConfig.bin')