import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BCM)

BLUE = 17
GREEN = 27
RED = 22

GPIO.setup(BLUE, GPIO.OUT)
GPIO.setup(GREEN, GPIO.OUT)
GPIO.setup(RED, GPIO.OUT)

for i in range(3):
    GPIO.output(BLUE, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(BLUE, GPIO.LOW)
    time.sleep(0.5)

for i in range(3):
    GPIO.output(GREEN, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(GREEN, GPIO.LOW)
    time.sleep(0.5)

for i in range(3):
    GPIO.output(RED, GPIO.HIGH)
    time.sleep(0.5)
    GPIO.output(RED, GPIO.LOW)
    time.sleep(0.5)

GPIO.cleanup()