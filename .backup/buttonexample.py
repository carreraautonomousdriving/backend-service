import RPi.GPIO as GPIO
import time




GPIO.setmode(GPIO.BCM)

GPIO.setup(23, GPIO.IN)

def button_callback():
    print("Button got pushed!")

lastValue = GPIO.input(23)

try:
    while True:
        if (GPIO.input(23) != lastValue): #Flanken Wechsel
            if (lastValue == GPIO.LOW): #Check for Low-High
                button_callback()
                time.sleep(0.1)
            
            lastValue = not lastValue

    

finally:
    GPIO.cleanup()