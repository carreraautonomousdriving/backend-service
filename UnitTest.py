import unittest, os

from RaceInstructionSet import RaceInstructionSet
from TrackInstruction import TrackInstruction

class UnitTest_SharedObjects(unittest.TestCase):
    def test_ris_init(self):
        ris = RaceInstructionSet(5)
        self.assertEqual(ris.Laps, 5)

    def test_ris_addInstruction(self):
        ris = RaceInstructionSet(0)
        ris.addInstruction(TrackInstruction(0.5, 500))
        ris.addInstruction(TrackInstruction(0.3, 250))
        self.assertEqual(ris.instructionCount, 2)

    def test_ris_resetIndex(self):
        ris = RaceInstructionSet(0)
        ris.ResetIndex = 5
        self.assertEqual(ris.ResetIndex, 5)

    def test_ris_serialize(self):
        ris = RaceInstructionSet(0)
        ris.addInstruction(TrackInstruction(0.2, 750))
        ris.serialize("./temp")
        assert os.path.exists("./temp")
        nris = RaceInstructionSet.deserialize("./temp")
        os.remove("./temp")
        self.assertEqual(nris.Laps, ris.Laps)
        self.assertEqual(nris.ResetIndex, ris.ResetIndex)
        self.assertEqual(nris.instructionList[0].Speed, 0.2)
        self.assertEqual(nris.instructionList[0].Ticks, 750)


if __name__ == '__main__':
    unittest.main()
