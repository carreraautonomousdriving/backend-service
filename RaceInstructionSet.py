import pickle
from TrackInstruction import TrackInstruction

class RaceInstructionSet:
    instructionCount = None
    instructionList = None
    Laps = None
    ResetIndex = None
    
    def __init__(self, rounds):
        self.instructionCount = 0
        self.instructionList = []
        self.Laps = rounds
        self.ResetIndex = -1

    def addInstruction(self, ti):
        self.instructionList.append(ti)
        self.instructionCount += 1

    def parseInstructionFile(self, path):
        self.instructionList.append(TrackInstruction(0, 40))
        self.instructionCount += 1

    def serialize(self, path):
        with open(path, 'wb') as outfile:
            pickle.dump(self, outfile)
    
    @classmethod
    def deserialize(self, path):
        with open(path, 'rb') as infile:
            return pickle.load(infile)