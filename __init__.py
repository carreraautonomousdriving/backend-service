import RPi.GPIO as GPIO
import time
import random

from RGBLED import RGBLED
from TrackInstruction import TrackInstruction
from RaceInstructionSet import RaceInstructionSet
from threading import Thread
from dual_max14870_rpi import motors, MAX_SPEED


#Setup and Port Konfiguration
STARTBUTTON           = 19
STOPBUTTON            = 26

LED_I_RPIN            = 2
LED_I_GPIN            = 3
LED_I_BPIN            = 4

LED_II_RPIN           = 17
LED_II_GPIN           = 27
LED_II_BPIN           = 22

LED_III_RPIN          = 10
LED_III_GPIN          = 9
LED_III_BPIN          = 11

LED_IV_RPIN           = 14
LED_IV_GPIN           = 15
LED_IV_BPIN           = 18

LED_V_RPIN            = 16
LED_V_GPIN            = 20
LED_V_BPIN            = 21

RedCC    = "FF0000"
YellowCC = "DDDD00"
GreenCC  = "00FF00"
OrangeCC = "FFA500"
BlueCC   = "0000FF"

ApplicationCycleTimeout = 0.5
TrafficLightTimeout = 0.8
ComputerAdvantageTimeout = 0.4
WarningCycleTimeout = 0.5
TickTimeout = 0.001

RaceThreadStopFlag = False
RaceThreadOverrunDetected = False



#Application Startup
GPIO.setmode(GPIO.BCM)
GPIO.setup(STARTBUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(STOPBUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

LED_I   = RGBLED(LED_I_RPIN,   LED_I_GPIN,   LED_I_BPIN)
LED_II  = RGBLED(LED_II_RPIN,  LED_II_GPIN,  LED_II_BPIN)
LED_III = RGBLED(LED_III_RPIN, LED_III_GPIN, LED_III_BPIN)
LED_IV  = RGBLED(LED_IV_RPIN,  LED_IV_GPIN,  LED_IV_BPIN)
LED_V   = RGBLED(LED_V_RPIN,   LED_V_GPIN,   LED_V_BPIN)



def startLight():
    global RaceThreadStopFlag
    LED_I.changeColor(RedCC)
    print("LED_I: red")
    time.sleep(TrafficLightTimeout)
    if(RaceThreadStopFlag):
        return

    LED_II.changeColor(RedCC)
    print("LED_II: red")
    time.sleep(TrafficLightTimeout)
    if(RaceThreadStopFlag):
        return

    LED_III.changeColor(RedCC)
    print("LED_III: red")
    time.sleep(TrafficLightTimeout)
    if(RaceThreadStopFlag):
        return

    LED_IV.changeColor(RedCC)
    print("LED_IV: red")
    time.sleep(TrafficLightTimeout)
    if(RaceThreadStopFlag):
        return

    LED_V.changeColor(RedCC)
    print("LED_V: red")
    time.sleep(TrafficLightTimeout)
    if(RaceThreadStopFlag):
        return

    time.sleep(random.randint(0, 6))
    if(RaceThreadStopFlag):
        return
    print("LEDs: green")
    LED_I.changeColor(GreenCC)
    LED_II.changeColor(GreenCC)
    LED_III.changeColor(GreenCC)
    LED_IV.changeColor(GreenCC)
    LED_V.changeColor(GreenCC)



def warningLight():
    LED_III.disable()
    for i in range(20):
        if(i % 2 == 0):
            LED_I.changeColor(BlueCC)
            LED_V.changeColor(BlueCC)
            LED_II.disable()
            LED_IV.disable()
        else:
            LED_II.changeColor(BlueCC)
            LED_IV.changeColor(BlueCC)
            LED_I.disable()
            LED_V.disable()
        time.sleep(WarningCycleTimeout)

    LED_II.disable()
    LED_IV.disable()



def startRace():
    global RaceThreadStopFlag
    global RaceThreadOverrunDetected
    startLight()
    if not RaceThreadStopFlag:
        tsi = RaceInstructionSet.deserialize("/opt/arerrac/currentTrack.bin")
        i, j, roundCounter = 0, 0, 0
        print(f"Start range {tsi.instructionCount}")
        while i < tsi.instructionCount:
            if(RaceThreadOverrunDetected):
                i = 1
                roundCounter += 1
            print(f"r:{str(roundCounter)} i:{str(i)}: changing speed to {str(tsi.instructionList[i].Speed)} for {str(tsi.instructionList[i].Ticks * TickTimeout)} s")
            while j < tsi.instructionList[i].Ticks:
                if(RaceThreadOverrunDetected):
                    break

                if(RaceThreadStopFlag):
                    break

                motors.motor1.setSpeed(MAX_SPEED * tsi.instructionList[i].Speed)
                if motors.getFault():
                    break

                time.sleep(TickTimeout)
                j += 1
            j = 0
            i += 1

            if(RaceThreadStopFlag):
                break


    motors.motor1.setSpeed(0)

    if RaceThreadStopFlag:
        warningLight()

    LED_I.disable()
    LED_II.disable()
    LED_III.disable()
    LED_IV.disable()
    LED_V.disable()

    print("end of startrace()")



def startButtonClickEventHandler(channel):
    print("startButtonClick: " + str(channel))

    if (channel == STARTBUTTON):
        global RaceThreadStopFlag
        RaceThreadStopFlag = False
        RaceThread.start()



def stopButtonClickEventHandler(channel):
   print("stopButtonClick: " + str(channel))

   if(channel == STOPBUTTON):
      global RaceThreadStopFlag
      RaceThreadStopFlag = True







RaceThread = Thread(target=startRace, args=())
GPIO.add_event_detect(STARTBUTTON,GPIO.FALLING,callback=startButtonClickEventHandler, bouncetime=2000)
GPIO.add_event_detect(STOPBUTTON,GPIO.FALLING,callback=stopButtonClickEventHandler, bouncetime=2000)



try:
    motors.setSpeeds(0, 0)
    while True:
        time.sleep(ApplicationCycleTimeout)
        if not RaceThread.is_alive():
            RaceThread = Thread(target=startRace, args=())



finally:
    motors.forceStop()
    GPIO.cleanup()
