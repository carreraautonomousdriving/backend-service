import RPi.GPIO as GPIO

class RGBLED:
    def __init__(self, redpin, greenpin, bluepin):
        self.rpwmpin = -1
        self.gpwmpin = -1
        self.bpwmpin = -1

        if(redpin != -1):
            GPIO.setup(redpin, GPIO.OUT)
            GPIO.output(redpin, GPIO.LOW)
            self.rpwmpin = GPIO.PWM(redpin, 1000)

        if(greenpin != -1):
            GPIO.setup(greenpin, GPIO.OUT)
            GPIO.output(greenpin, GPIO.LOW)
            self.gpwmpin = GPIO.PWM(greenpin, 1000)

        if(bluepin != -1):
            GPIO.setup(bluepin, GPIO.OUT)
            GPIO.output(bluepin, GPIO.LOW)
            self.bpwmpin = GPIO.PWM(bluepin, 1000)



    def changeColor(self, hexcode):
        if(self.rpwmpin != -1):
            self.rpwmpin.start( (int((hexcode[0] + hexcode[1]), 16) / 2.55) )

        if(self.gpwmpin != -1):
            self.gpwmpin.start( (int((hexcode[2] + hexcode[3]), 16) / 2.55) )

        if(self.bpwmpin != -1):
            self.bpwmpin.start( (int((hexcode[4] + hexcode[5]), 16) / 2.55) )



    def disable(self):
        self.changeColor("000000")
